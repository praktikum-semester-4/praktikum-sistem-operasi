#!/bin/bash

# ANSI bold and reset 
BOLD="\033[1m"
RESET="\033[0m"

# Create directory if the directory doesn't exist
if [ ! -d "$HOME/.pac" ]; then
  mkdir "$HOME/.pac"
fi

# Function to copy helper script to user home directory
copy_script()
{
    echo -e "Your package manager: ${BOLD}'${1}'${RESET}"
    echo -e "Pac will be installed with ${BOLD}'${1}'${RESET} as package manager"
    cp ./helper-script/${1}_script.sh $HOME/.pac/pac.sh
}

# Check the package manager
if command -v apt-get >/dev/null 2>&1; then
    copy_script "apt"
elif command -v dnf >/dev/null 2>&1; then
    copy_script "dnf"
elif command -v yum >/dev/null 2>&1; then
    copy_script "yum"
elif command -v pacman >/dev/null 2>&1; then
    copy_script "pacman"
elif command -v zypper >/dev/null 2>&1; then
    copy_script "zypper"
else
    echo -e "Your package manager is not yet supported by ${BOLD}Pac${RESET}"
fi

# Change permission and add alias to shell (bash and zsh)
if [ -e "$HOME/.pac/pac.sh" ]; then
    chmod +x $HOME/.pac/pac.sh
    if ! grep -q "alias pac='/home/symz/.pac/pac.sh'" ~/.bashrc || ! grep -q "alias pac='/home/symz/.pac/pac.sh'" ~/.zshrc; then
        echo "" >> ~/.bashrc
        echo "" >> ~/.zshrc
        echo "alias pac='/home/symz/.pac/pac.sh'" >> ~/.bashrc
        echo "alias pac='/home/symz/.pac/pac.sh'" >> ~/.zshrc
    fi

    # Load and restart the configuration
    source ~/.bashrc
    source ~/.zshrc
    echo "Pac installed successfully"
fi