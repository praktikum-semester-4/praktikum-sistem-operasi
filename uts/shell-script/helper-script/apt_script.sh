#!/bin/bash

case "$1" in
    "i"|"in"|"install") # Install package
    sudo apt-get install "$2"
    ;;
    "upt"|"update") # Update package repository
    sudo apt-get update
    ;;
    "upg"|"upgrade") # Upgrade package
    sudo apt-get upgrade
    ;;
    "rm"|"remove") # Remove package and dependency
    if [ "$2" = "pac" ]; then
        rm -rf "$HOME/.pac"
        echo "Pac uninstalled successfully"
    else
        sudo apt-get remove "$2"
    fi
    ;;
    "frm"|"fullrm"|"fullremove") # Like rm and remove configuration
    sudo apt-get purge "$2"
    ;;
    "arm"|"autorm"|"autoremove") # Auto remove unused package
    sudo apt-get autoremove
    ;;
    "cl"|"clean") # Delete cache and downloaded package from repo
    sudo apt-get clean
    ;;
    "sc"|"search") # Search package
    apt search "$2"
    ;;
    "ls"|"list") # List all installed package
    dpkg -l
    ;;
    "-h"|"help") # Show Pac usage
    echo -e "install, in, i"
    echo -e "update, upt"
    echo -e "upgrade, upg"
    echo -e "remove, rm"
    echo -e "fullremove, fullrm, frm"
    echo -e "autoremove, autorm, arm"
    echo -e "clean, cl"
    echo -e "search, sc"
    echo -e "list, ls"
    echo -e "help, -h"
    ;;
    *)
    echo "Invalid command"
    ;;
esac