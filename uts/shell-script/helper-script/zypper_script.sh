#!/bin/bash

case "$1" in
    "i"|"in"|"install") # Install package
    sudo zypper install "$2"
    ;;
    "upt"|"update") # Update package repository
    sudo zypper update
    ;;
    "upg"|"upgrade") # Upgrade package
    sudo zypper upgrade
    ;;
    "rm"|"remove") # Remove package and dependency
    if [ "$2" = "pac" ]; then
        rm -rf "$HOME/.pac"
        echo "Pac uninstalled successfully"
    else
        sudo zypper remove "$2"
    fi
    ;;
    "frm"|"fullrm"|"fullremove") # Like rm and remove configuration
    sudo zypper remove "$2" --clean-deps
    ;;
    "arm"|"autorm"|"autoremove") # Auto remove unused package
    sudo zypper packages --unneeded | awk '{print $4}' | sudo xargs zypper remove -y
    ;;
    "cl"|"clean") # Delete cache and downloaded package from repo
    sudo zypper clean
    ;;
    "sc"|"search") # Search package
    sudo zypper search "$2"
    ;;
    "ls"|"list") # List all installed package
    sudo zypper list installed
    ;;
    "-h"|"help") # Show Pac usage
    echo -e "install, in, i"
    echo -e "update, upt"
    echo -e "upgrade, upg"
    echo -e "remove, rm"
    echo -e "fullremove, fullrm, frm"
    echo -e "autoremove, autorm, arm"
    echo -e "clean, cl"
    echo -e "search, sc"
    echo -e "list, ls"
    echo -e "help, -h"
    ;;
    *)
    echo "Invalid command"
    ;;
esac