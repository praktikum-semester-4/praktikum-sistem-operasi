#!/bin/bash

case "$1" in
    "i"|"in"|"install") # Install package
    sudo yum install "$2"
    ;;
    "upt"|"update") # Update package
    sudo yum update
    ;;
    "upg"|"upgrade") # Upgrade linux version
    sudo yum upgrade
    ;;
    "rm"|"remove") # Remove package and dependency
    if [ "$2" = "pac" ]; then
        rm -rf "$HOME/.pac"
        echo "Pac uninstalled successfully"
    else
        sudo yum remove "$2"
    fi
    ;;
    "frm"|"fullrm"|"fullremove") # Like rm and remove configuration
    sudo yum remove "$2" --config-files
    ;;
    "arm"|"autorm"|"autoremove") # Auto remove unused package
    sudo yum autoremove
    ;;
    "cl"|"clean") # Delete cache and downloaded package from repo
    sudo yum clean all
    ;;
    "sc"|"search") # Search package
    yum search "$2"
    ;;
    "ls"|"list") # List all installed package
    yum list installed
    ;;
    "-h"|"help") # Show Pac usage
    echo -e "install, in, i"
    echo -e "update, upt"
    echo -e "upgrade, upg"
    echo -e "remove, rm"
    echo -e "fullremove, fullrm, frm"
    echo -e "autoremove, autorm, arm"
    echo -e "clean, cl"
    echo -e "search, sc"
    echo -e "list, ls"
    echo -e "help, -h"
    ;;
    *)
    echo "Invalid command"
    ;;
esac