#!/bin/bash

case "$1" in
    "i"|"in"|"install") # Install
    sudo dnf install "$2"
    ;;
    "upt"|"update") # Update repo
    sudo dnf update
    ;;
    "upg"|"upgrade") # Upgrade package
    sudo dnf upgrade
    ;;
    "rm"|"remove") # Remove package and dependency
    if [ "$2" = "pac" ]; then
        rm -rf "$HOME/.pac"
        echo "Pac uninstalled successfully"
    else
        sudo dnf remove "$2"
    fi
    ;;
    "frm"|"fullrm"|"fullremove") # Like rm and remove configuration
    sudo dnf remove "$2" --setopt=clean_requirements_on_remove=1
    ;;
    "arm"|"autorm"|"autoremove") # Auto remove unused package
    sudo dnf autoremove
    ;;
    "cl"|"clean") # Delete cache and downloaded package from repo
    sudo dnf clean all
    ;;
    "sc"|"search") # Search package
    dnf search "$2"
    ;;
    "ls"|"list") # List all installed package
    sudo dnf list installed
    ;;
    "-h"|"help") # Show Pac usage
    echo -e "install, in, i"
    echo -e "update, upt"
    echo -e "upgrade, upg"
    echo -e "remove, rm"
    echo -e "fullremove, fullrm, frm"
    echo -e "autoremove, autorm, arm"
    echo -e "clean, cl"
    echo -e "search, sc"
    echo -e "list, ls"
    echo -e "help, -h"
    ;;
    *)
    echo "Invalid command"
    ;;
esac