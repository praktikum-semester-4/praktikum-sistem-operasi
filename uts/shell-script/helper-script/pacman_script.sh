#!/bin/bash

case "$1" in
    "i"|"in"|"install") # Install package
    sudo pacman -S "$2"
    ;;
    "upt"|"update") # Update package repository
    sudo pacman -Sy
    ;;
    "upg"|"upgrade") # Upgrade package
    sudo pacman -Syu
    ;;
    "rm"|"remove") # Remove package and dependency
    if [ "$2" = "pac" ]; then
        rm -rf "$HOME/.pac"
        echo "Pac uninstalled successfully"
    else
        sudo pacman -Rs "$2"
    fi
    ;;
    "frm"|"fullrm"|"fullremove") # Like rm and remove configuration
    sudo pacman -Rns "$2"
    ;;
    "arm"|"autorm"|"autoremove") # Auto remove unused package
    echo "pacman didn't have autoremove feature, use remove/fullremove instead."
    ;;
    "cl"|"clean") # Delete cache and downloaded package from repo
    sudo pacman -Sc
    ;;
    "sc"|"search") # Search package
    pacman -Ss "$2"
    ;;
    "ls"|"list") # List all installed package
    pacman -Q
    ;;
    "-h"|"help") # Show Pac usage
    echo -e "install, in, i"
    echo -e "update, upt"
    echo -e "upgrade, upg"
    echo -e "remove, rm"
    echo -e "fullremove, fullrm, frm"
    echo -e "autoremove, autorm, arm"
    echo -e "clean, cl"
    echo -e "search, sc"
    echo -e "list, ls"
    echo -e "help, -h"
    ;;
    *)
    echo "Invalid command"
    ;;
esac