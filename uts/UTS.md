# NO.1
## Monitoring Resource
### RAM
Untuk memonitor penggunaan memori RAM, bisa menggunakan command `free`. Contoh output:
```bash
[symz@symz-manjaro ~]$ free
               total        used        free      shared  buff/cache   available
Mem:         7860144     3282724     2105912      458388     2471508     3853672
Swap:        8796560           0     8796560
```
Secara default satuan yang ditampilkan adalah kilobyte, kita bisa menggunakan flag `--mega` agar ditampilkan dalam satuan megabyte. Contoh ouptput:
```bash
[symz@symz-manjaro ~]$ free --mega
               total        used        free      shared  buff/cache   available
Mem:            8048        3537        1915         471        2595        3760
Swap:           9007           0        9007
```
### CPU
Untuk memonitor penggunaan CPU, bisa menggunakan command `top` atau `ps -aux`. Contoh output:

![top](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/raw/main/screenshot/top.png)
![ps -aux](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/raw/main/screenshot/ps-aux.png)

### Storage
Untuk memonitor penggunaan Storage, bisa menggunakan command `lsblk` atau `df`. Contoh output:
```bash
[symz@manja-chan ~]$ lsblk
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
sda      8:0    0 119,2G  0 disk 
├─sda1   8:1    0   512M  0 part /boot/efi
├─sda2   8:2    0 110,4G  0 part /
└─sda3   8:3    0   8,4G  0 part [SWAP]
[symz@manja-chan ~]$ df
Filesystem     1K-blocks     Used Available Use% Mounted on
dev              3915528        0   3915528   0% /dev
run              3930072     1444   3928628   1% /run
/dev/sda2      113309924 25306532  82201408  24% /
tmpfs            3930072     4400   3925672   1% /dev/shm
tmpfs            3930072     2972   3927100   1% /tmp
/dev/sda1         523244      312    522932   1% /boot/efi
tmpfs             786012       76    785936   1% /run/user/1000
```
Sebenarnya ada salah satu tools yang bisa memonitor semuanya dengan mudah yaitu dengan menggunakan `btop`, tetapi kita harus menginstall package `btop` terlebih dahulu sebelum bisa menggunakannya. Contoh output:

![btop](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/raw/main/screenshot/btop.png)

Dengan menggunakan btop kita bisa memonitor CPU, RAM, Storage, Network dan juga bisa melakukan `kill` pada program yang sedang berjalan.

# NO.2
## Manajemen Program
### Monitor program yang berjalan
Untuk memonitor program yang berjalan, bisa menggunakan command yang sama seperti sebelumnya yaitu menggunakan `top`, `htop`, atau `btop`.

### Menhentikan program yang berjalan
Untuk menghentikan program yang berjalan bisa menggunakan command `kill`, lalu masukkan no PID nya, contoh:
```bash
kill <PID>
```

### Otomasi perintah dengan shell script
Pertama perikasa terlebih dahulu shell yang sedang digunakan dengan perintah:
```bash
echo $SHELL
```
Contoh output:
```bash
[symz@manja-chan ~]$ echo $SHELL
/bin/bash
```
Lalu buat file scriptnya dengan perintah:
```bash
touch script.sh
```
Buka file scriptnya dengan text editor apapun lalu isikan pada baris pertama sesuai dengan output dari `echo $SHELL` tadi, dan tambahkan `#!`.
```bash
#! /bin/bash
```
Perintah di atas berfungsi untuk memberitahu dengan apa script akan dijalankan, pada kasus di atas adalah dengan `bash`.

Selanjutnya tinggal memasukkan perintah-perintah bash yang akan diotomasi. Contohnya kita akan memasukkan perintah untuk membuat file dengan mengambil nama dari parameter.
```bash
#! /bin/bash

touch $1
```
Untuk mengeksekusi script-nya kita harus mengganti permission agar bisa dieksekusi.
```bash
chmod u+x nama_script.sh
```
Lalu tinggal eksekusi dengan perintah. Jika script berada di direktori lain sesuaikan saja direktorinya.
```bash
<direktori>/nama_script.sh
```

### Penjadwalan eksekusi program dengan cron
**Note**: Khusus untuk pengguna linux dengan turunan Arch tidak ada `cron` secara default, karena telah diganti dengan menggunakan `systemd timer`, oleh karena itu kita bisa menggunakan package yang mengimplementasikan `cron`, di sini kita akan menggunakan `cronie`.
```bash
sudo pacman -S cronie
```
Lalu jalankan service nya dengan perintah berikut.
```bash
sudo systemctl start cronie.service
```
Maka kita bisa menggunakan `cron`, jangan lupa untuk memastikan servicenya berjalan.

Saat pertamakali menggunakan `cron`, buat `crontab` nya terlebih dahulu dengan command ini
```bash
crontab -e
```
cara penggunaan `crontab` adalah sebagai berikut:
```bash
* * * * * command
- - - - -
| | | | |
| | | | ----- Hari dalam seminggu (0 - 7) (Minggu adalah 0 atau 7)
| | | ------- Bulan (1 - 12)
| | --------- Hari dalam bulan (1 - 31)
| ----------- Jam (0 - 23)
------------- Menit (0 - 59)
```
pada bagian command bisa diisi dengan file script yang akan dijalankan. Contohnya sebagai berikut.
```bash
* * * * * date >> /home/symz/Documents/date.txt
```
Perintah di atas akan dijalankan setiap menit, dan akan memasukkan output dari perintah `date` ke file `date.txt`.

# NO.3
## Manajemen Network
### Akses sistem operasi dengan SSH
Untuk mengakses sistem operasi dengan SSH adalah menggunakan command berikut:
1. Akses dengan ip
    ```bash
    ssh <account>@<public-ip>
    ```
    - account = username
    - public-ip = ip public tujuan
2. Akses dengan port
    ```bash
    ssh <account>@<public-ip> -p <port>
    ```
    - port = port dari ssh yang dituju (default port 22)

![ssh-connect](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/raw/main/screenshot/ssh-connect.gif)

### Monitor program yang menggunakan network
Untuk memonitor program yang sedang menggunakan network, kita bisa menggunakan program `nethogs`, sebelum menggunakannya install terlebih dahulu aplikasi `nethogs`. Contoh ouput:

![nethogs](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/raw/main/screenshot/nethogs.png)

### Mengoperasikan HTTP client
Curl GET
```bash
curl <link-url>
```
Contoh
```bash
curl  https://api.jikan.moe/v4/anime?q=naruto
```
Request di atas akan mengembalikan JSON yang berisi hasil pencarian anime yang berjudul naruto.

# NO.4
## Manajemen File dan Folder
### Navigasi
Untuk navigasi bisa menggunakan command berikut:
```bash
cd
ls
pwd
```
- `cd`, untuk mengganti direktori
- `ls`, menampilkan semua list file dan folder yang ada di direktori yang sedang ditempati
- `pwd`, menampilkan direktori yang sedang ditempati

### CRUD file dan folder
Untuk CRUD bisa menggunakan command berikut:
```bash
mkdir
touch
rm
mv
cp
```
- `mkdir`, untuk membuat direktori
- `touch`, untuk membuat file
- `rm`, menghapus file atau direktori
- `mv`, cut/move file atau direktori
- `cp`, copy file atau direktori

### Manajemen ownership
Ada dua command dasar dalam manajemen ownership yaitu `chmod` dan `chown`.
Berikut ini adalah contoh penggunaannya:
```bash
chmod [option] <file>
```
- `chmod`, digunakan untuk memodifikasi permission suatu file.
- `[option]` berisi perintah untuk mengubah permission, klik [disini](https://gitlab.com/marchgis/march-ed/2023/courses/if214009-praktikum-sistem-operasi/-/tree/main/linux#file-folder-mode-and-ownership) untuk melihat lebih detail.
- `<file>` berisi nama file yang akan diubah permissionnya.
```bash
chown [user:group] <file>
```
- `chown`, digunakan untuk memodifikasi owner dari suatu file.
- `[user:group]`, berisi nama user/group mana yang akan menjadi ownernya.
- `<file>`, berisi nama file yang akan diubah ownernya.

### Pencarian file dan folder
Untuk mencari file dan folder bisa menggunakan `find` atau `grep`. Contoh penggunaan `find`:
```bash
find <path> <conditions> <actions>
```
- `<directory>`, diisi dengan memasukkan direktori yang akan dicari file dan foldernya.
- `<conditions>`, diisi dengan kondisi yang akan dipilih, klik [disini](https://devhints.io/find) untuk lihat lebih lengkap.
- `<actions>`, diisi dengan actions yang akan dilakukan.

### Kompresi data
Untuk melakukan kompresi data, bisa menggunakan command `tar` dan `gzip`, berikut cara penggunaanya:
```bash
tar [options] [archive-file] [file or directory to be archived]
```
- `[options]`, diisi dengan options yang akan digunakan. klik [disini](https://www.geeksforgeeks.org/tar-command-linux-examples/) untuk lihat lebih lengkap.
- `[archive-file]`, nama archive yang akan dibuat.
- `[file or directory to be archived]`, file atau direktori yang akan diarchive.
```bash
gzip [options] [filenames]
```
- `[options]`, diisi dengan options yang akan digunakan. klik [disini](diisi dengan options yang akan digunakan. klik [disini](https://www.geeksforgeeks.org/tar-command-linux-examples/) untuk lihat lebih lengkap.
- `[filenames]`, diisi dengan nama file yang akan dicompress.

Contoh cara menggunakannya:
- `tar`
![tar](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/raw/main/screenshot/tar.gif)
- `gzip`
![gzip](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/raw/main/screenshot/gzip.gif)

# NO.5
Shell script source code: [Pac.sh](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/tree/main/shell-script)

![shell-script]()

# NO.6
Demo program shell script: [Youtube](https://youtu.be/fV-4E21scTA)

# NO.7
Maze game link: [Musashi-World](https://maze.informatika.digital/maze/musashi-world/)

![maze-game]()

# NO.8
Operating System:
- [Manjaro](https://manjaro.org) (Desktop Environment: GNOME)
- Display Server:
    - [Wayland](https://wayland.freedesktop.org/), display server utama buat sehari-hari
    - [Xorg](https://wiki.archlinux.org/title/xorg), display server kedua buat dipake record atau share screen di gmeet/zoom

Penggunaan Tools:
- [BTOP](https://github.com/aristocratos/btop) (Monitoring Resource)
- [NetHogs](https://github.com/raboof/nethogs) (Monitoring Network)
- [Cronie](https://github.com/cronie-crond/cronie) (Daemon untuk menjalankan Cronjob)
- [Peek](https://github.com/phw/peek) (Untuk record screen sebagai gif)
- [OBS Studio Linux](https://obsproject.com/download) (Untuk record screen)

# NO.9
Dokumentasi: [README.md](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/blob/main/README.md)

