# 01 - Setting Dockerfile

## Install dependensi/package yang dibutuhkan

Instalasi package hanya dilakukan di image yang menggunakan base **Alpine Linux** saja, karena di image dengan base **Scratch** hanya membutuhkan file binary programnya saja.

```dockerfile
RUN apk update && apk add git vim netcat-openbsd
```

Perintah di atas akan melakukan update list package terlebih dahulu, lalu install package `git`, `vim`, dan `netcat-openbsd`.

## Deploy web service ke dalam docker images

- Scratch Image

    Pada **Scratch Image** kita cukup build saja file binary-nya, lalu copy file binary hasil build nya ke dalam sebuah image (bisa menggunakan base image lain juga, tetapi disini yang digunakan adalah **Scratch Image**)

    ```dockerfile
    FROM scratch
    MAINTAINER symz911
    COPY lokally-api / 
    CMD ["./lokally-api"]
    ```

- Alpine Image

    Pada image yang menggunakan base **Alpine Linux**, kita akan copy seluruh folder projectnya, lalu install dependensi yang diperlukan oleh project, melakukan build, dan terakhir run file binary-nya. Sebenarnya setelah copy folder projectnya, kita bisa langsung menjalankan programnya dengan melakukan run pada file `main.go` nya langsung.

    ```dockerfile
    FROM golang:1.20-alpine
    MAINTAINER symz911
    WORKDIR /app
    COPY . .
    RUN go mod tidy
    RUN go build -o lokally-api
    CMD ["./lokally-api"]
    ```

    Jangan lupa untuk memasukkan perintah untuk menambah package/dependensi yang diperlukan (jika ada) contohnya:

    ```dockerfile
    FROM golang:1.20-alpine
    MAINTAINER symz911
    WORKDIR /app
    RUN apk update && apk add git vim netcat-openbsd
    COPY . .
    RUN go mod tidy
    RUN go build -o lokally-api
    CMD ["./lokally-api"]
    ```

# 02 - Dockerfile

Link dockerfile:

- Scratch Image: [Scratch Dockerfile - Gitlab](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/blob/main/uas/scratch/Dockerfile)

- Alpine Image: [Alpine Dockerfile - Gitlab](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/blob/main/uas/alpine/Dockerfile)

# 03 - Web Service

Web service berbentuk REST API yang mengirimkan data JSON, API bisa diakses di [3.1.27.254:25103/api/](http://3.1.27.254:25103/api/), untuk endpoint dan method yang tersedia bisa dilihat di link readme dibagian [**05 - Deskripsi Project**](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/blob/main/uas/UAS.md#05-deskripsi-project).

Link repository project web service: [Web Service - Gitlab](https://gitlab.com/project4921918/lokal.ly/lokally-api/-/tree/main/)

# 04 - Docker Compose

Untuk bagian `docker-compose.yaml` isinya hampir sama, hanya berbeda di pemilihan image nya saja.

Link docker compose: 

- Scratch Image: [Scratch Docker Compose - Gitlab](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/blob/main/uas/scratch/docker-compose.yaml)

- Alpine Image: [Alpine Docker Compose - Gitlab](https://gitlab.com/praktikum-semester-4/praktikum-sistem-operasi/-/blob/main/uas/alpine/docker-compose.yaml)

# 05 - Deskripsi Project

Link readme project: [Readme - Gitlab](https://gitlab.com/project4921918/lokal.ly/lokally-api/-/blob/main/README.md)

# 06 - Demo Web Service

Link demo project: [Demo - Youtube](https://youtu.be/-DKjr4Lneu0)

# 07 - Docker Hub

- General: [General - Docker Hub](https://hub.docker.com/repository/docker/symz911/lokally_api/general)

- Scratch Image: [Scratch Image - Docker Hub](https://hub.docker.com/layers/symz911/lokally_api/1.0/images/sha256-53b544c1a19a8258cd90aa7ded14f9d1750caa6b943f135ebd3096dd0ea6c1eb?context=repo)

- Alpine Image: [Alpine Image - Docker Hub](https://hub.docker.com/layers/symz911/lokally_api/1.0-alpine/images/sha256-29877522fc2ae7df5d23f84627d144309a97622989e44c914df5c3a873e90b2d?context=repo)
